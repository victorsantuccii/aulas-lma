const {createApp} = Vue;
createApp({

    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,
            
            // vetor de imagens locais
            imagensLocais:[
                './imagens/lua.jpg',
                './imagens/SENAI_logo.png',
                './imagens/sol.jpg'
            ],

            imagensInternet:[
                'https://i.pinimg.com/originals/0c/27/58/0c27583e69d7bbbbf514ad017cf923a5.jpg',
                'https://stickerly.pstatic.net/sticker_pack/0Las2qYYJEdsPfAKBItmg/R3I04F/31/fa966e28-fed5-4e37-bd7b-8c2a2339d43e.png',
                'https://i.ytimg.com/vi/ef2tQzAobys/mqdefault.jpg',
                'https://i.pinimg.com/736x/7d/7d/15/7d7d155eb38ce05062d33ec26e06d615.jpg',
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTT9XIe-GHXm1vKUapS5r8wzmxrVhkGjT5PUbAT3TmzZ8U8CHc7GVc_i93oymcoqfExA7U&usqp=CAU',
                'https://img.ifunny.co/images/b60a325e45841612ac01a892ea9cb32349f515fd5680aa11e387eeab5767de8a_1.jpg'
            ],

        }; // fim return
    }, // Fim data

    computed:{

        randomImage()
        {
            return this.imagensLocais[this.randomIndex];
        },

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        }

    }, // fim computed
    
    methods:{

        getRandomImage()
        {
            this.randomIndex = Math.floor(Math.random()*this.imagensLocais.length);

            this.randomIndexInternet = Math.floor(Math.random()*this.imagensInternet.length);
        }

    } // fim methods

}).mount("#app");