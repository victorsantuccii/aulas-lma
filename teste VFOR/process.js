const {createApp} = Vue;

createApp({
    data(){
        return{
            show: false,
            itens:["Bola", "Bolsa", "Tenis"],
        }; // Return
    }, // Data

    methods:{
        showItens: function(){
            this.show = !this.show;
        }, // Fechamento Showitens
    }, // Methods


}).mount("#app"); // Fechamento CreateApp