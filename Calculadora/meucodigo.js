const {createApp} = Vue;
createApp
({

    data()
    {

        return{
            display:"0",
            operandoAtual:null,
            operandoAnterior:null,
            operador:null,


        } // Fechamento do Return
            
    }, // Fechamento da função "Data"

    methods:{

        handleButtonClick(botao)
        {
            switch(botao)
            {
                case "+":
                case "-":
                case "/":
                case "*":
                    
                    this.handleOperation(botao);
                    break;    

                case ".":
                    this.handleDecimal();
                    break;

                case "=":
                    this.handleEquals(botao);
                    break;

                default: this.handleNumber(botao);
                break;
            } // fechamendo do switch
        }, // fechamento do handlebuttonclick

        handleNumber(numero)
        {
          if(this.display === "0")
          {
            this.display = numero.toString();

          } // fechamento do If


          else
          {
            this.display += numero.toString();

          }  // fechamento do Else

        }, // fechamendo handleNumber

        handleOperation(operacao){
            if(this.operandoAtual !== null)
            {
                this.handleEquals();

            }  // fechamento do if 

            this.operador = operacao;

            this.operandoAtual = parseFloat(this.display);
            this.display = "0";


        }, // fechamento handleOperation

        handleEquals(){
            const displayAtual = parseFloat (this.display);
            if(this.operandoAtual !== null && this.operador !== null)
            {
                switch(this.operador)
                {
                    case "+":
                        this.display = (this.operandoAtual + displayAtual).toString();
                        break;
                    case "-":
                        this.display = (this.operandoAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.operandoAtual * displayAtual).toString();
                        break;
                    case "/":
                        this.display = (this.operandoAtual / displayAtual).toString();
                        break;
                } // Fim do Switch

                this.operandoAnterior = this.operandoAtual;
                this.operandoAtual = null;
                this.operador =  null;

            } //Fechamento do if
            
            else
            {
                this.operandoAnterior = displayAtual;
            } // Fechamento do Else

        }, // Fechamento handleEquals

        handleDecimal(){
            if(!this.display.includes("."))
            {

                this.display += ".";

            } // fechamento if


        }, // Fechamento handleDecimal

        handleClear(){

            this.display = "0";
            this.operandoAtual = null;
            this.operandoAnterior = null;
            this.operador = null;

        }, // fim handleClear


    }, // fim methods

}).mount("#app"); // Fechamento do "createApp"

